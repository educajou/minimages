const inputLargeur = document.getElementById('largeur');
const inputHauteur = document.getElementById('hauteur');
const divApropos = document.getElementById('div-a-propos');
const darkbox = document.getElementById('darkbox');


function ouvre(div){
    div.classList.remove('hide');
    darkbox.classList.remove('hide');
}


function ferme(div){
    div.classList.add('hide');
    darkbox.classList.add('hide');
}


$(document).ready(function() {




    inputLargeur.value = 800;
    inputHauteur.value = 800;



    // Fonction principale pour initialiser l'application
    function init() {
        $('#images').on('change', afficherImages);
        $('#reduire-zip').on('click', reduireEtTelechargerZip);
        $('#reduire-images').on('click', reduireEtTelechargerImagesSeparees);
    }

    // Affiche les images sélectionnées avec une taille maximale de 200px
    function afficherImages() {
        const fichiers = $('#images')[0].files;
        $('#resultat').empty();

        Array.from(fichiers).forEach((fichier) => {
            const lecteur = new FileReader();
            lecteur.onload = function(e) {
                const img = new Image();
                img.src = e.target.result;
                img.style.maxWidth = '200px';
                img.style.maxHeight = '200px';
                $('#resultat').append(img);
            }
            lecteur.readAsDataURL(fichier);
        });
    }

    // Gestionnaire de clic pour réduire et télécharger les images
    function reduireEtTelechargerZip() {
        const fichiers = $('#images')[0].files;
        const largeurMax = parseInt($('#largeur').val());
        const hauteurMax = parseInt($('#hauteur').val());

        if (fichiers.length === 0) {
            alert('Veuillez sélectionner des images.');
            return;
        }

        const zip = new JSZip();
        const dossierImages = zip.folder("images");
        let compteurTermine = 0;

        Array.from(fichiers).forEach((fichier) => {
            const lecteur = new FileReader();
            lecteur.onload = function(e) {
                const img = new Image();
                img.src = e.target.result;
                img.onload = function() {
                    const canvas = redimensionnerImage(img, largeurMax, hauteurMax);
                    canvas.toBlob(function(blob) {
                        ajouterImageAuDossier(dossierImages, blob, fichier.name);
                        compteurTermine++;

                        if (compteurTermine === fichiers.length) {
                            zip.generateAsync({ type: 'blob' }).then(function(content) {
                                saveAs(content, 'images.zip');
                            });
                        }
                    }, fichier.type, 0.8);
                }
            }
            lecteur.readAsDataURL(fichier);
        });
    }

    function reduireEtTelechargerImagesSeparees() {
        const fichiers = $('#images')[0].files;
        const largeurMax = parseInt($('#largeur').val());
        const hauteurMax = parseInt($('#hauteur').val());

        if (fichiers.length === 0) {
            alert('Veuillez sélectionner des images.');
            return;
        }

        Array.from(fichiers).forEach((fichier) => {
            const lecteur = new FileReader();
            lecteur.onload = function(e) {
                const img = new Image();
                img.src = e.target.result;
                img.onload = function() {
                    const canvas = redimensionnerImage(img, largeurMax, hauteurMax);
                    canvas.toBlob(function(blob) {
                        const url = URL.createObjectURL(blob);
                        const lien = document.createElement('a');
                        lien.href = url;
                        lien.download = 'redim-' + fichier.name;
                        lien.click();
                        URL.revokeObjectURL(url);
                    }, fichier.type, 0.8);
                }
            }
            lecteur.readAsDataURL(fichier);
        });
    }


    // Redimensionne une image tout en conservant les proportions et retourne un canvas
    function redimensionnerImage(image, largeurMax, hauteurMax) {
        const canvas = document.createElement('canvas');
        const ctx = canvas.getContext('2d');

        let largeur = image.width;
        let hauteur = image.height;

        // Calculer la nouvelle taille en conservant les proportions
        if (largeur > hauteur) {
            if (largeur > largeurMax) {
                hauteur *= largeurMax / largeur;
                largeur = largeurMax;
            }
        } else {
            if (hauteur > hauteurMax) {
                largeur *= hauteurMax / hauteur;
                hauteur = hauteurMax;
            }
        }

        canvas.width = largeur;
        canvas.height = hauteur;

        ctx.drawImage(image, 0, 0, largeur, hauteur);
        return canvas;
    }

    // Ajoute une image redimensionnée au dossier ZIP
    function ajouterImageAuDossier(dossier, blob, nomFichier) {
        const nomRedimensionne = 'redim-' + nomFichier;
        dossier.file(nomRedimensionne, blob);
    }

    // Initialisation de l'application
    init();
});
